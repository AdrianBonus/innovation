#!/usr/bin/env python
from __future__ import division
import rospy
from std_msgs.msg import *
from sensor_msgs.msg import LaserScan
import sys
from geometry_msgs.msg import Twist, Vector3
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import cv2
import numpy as np
from time import sleep
import time


#create publishers
pub = rospy.Publisher('Distance', Float32, queue_size=10)
rec_publishher = rospy.Publisher('RecPoints', Int8, queue_size=10)
bridge = CvBridge()

#listen to the topics needed
def listener():
    rospy.init_node('listener', anonymous=True)
    sub = rospy.Subscriber('/scan', LaserScan, callback)
    sub2 = rospy.Subscriber('Distance', Float32, callback2)
    #continue listening
    rospy.spin()

#this is for hte distance
def callback(msg):
    #there are 666 laser beams, 333 is the one in the middle
    laser_reading_front = msg.ranges[333]
    #publish
    pub.publish(laser_reading_front)
    #rospy.loginfo(laser_reading_front)

def callback2(msg):
    #rospy.loginfo(msg.data)
    number_of_match = recognise()
    rec_publishher.publish(number_of_match)


#this is the tamplate matching
def recognise():
    #open the images
    img_from_camera = cv2.imread('door_handle.png',0)
    image_of_handle = cv2.imread('door_handle_template.png',0)
    #create an orb extractor
    orb = cv2.ORB_create(10000, 1.2, nlevels=9, edgeThreshold=4)

    #find the keypoints and describe them
    camera_key_points, camera_key_description = orb.detectAndCompute(img_from_camera,None)
    handle_key_points, handle_key_description = orb.detectAndCompute(image_of_handle,None)

    FLANN_INDEX_LSH = 6

    index_params = dict(algorithm = FLANN_INDEX_LSH,
                        table_number = 6,
                        key_size=12,
                        multi_probe_level=1)
    search_params = dict(check=100)
    flann=cv2.FlannBasedMatcher(index_params, search_params)
    matches = flann.knnMatch(camera_key_description,handle_key_description,k=2)
    # Apply ratio test
    good_match = 0#this variable is used to count the number of good matches
    good = []
    for m_n in matches:#each thing in the list must be checked if there are 2 neighbours
        if len(m_n)!=2:
            continue
        (m,n)=m_n
        if m.distance < 0.75*n.distance:#if ratio is close to 1, they are equal
             good.append([m])
             good_match = good_match+1
    return len(good)






if __name__ == '__main__':
    try :
        listener()
    except :
        pass
