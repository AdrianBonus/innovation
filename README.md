Installation :
1) Please ensure that you have ROS and ROS TIAGo along with their dependecies installed by following the tutorial in this link :
  ROS : http://wiki.ros.org/kinetic/Installation/Ubuntu
  TIAGo : http://wiki.ros.org/Robots/TIAGo/Tutorials/Installation/TiagoSimulation

2) After succesful installation please clone my repository under the TIAGo folder, src. For example :
   /home/username/tiago_public_ws/src

3) Go into the folder and execute the command <catkin build>

4) On the world folder, please copy the content to :
   /home/username/tiago_public_ws/src/tiago_simulation/tiago_gazebo
   where tiago_public_ws is your installation folder

5) Download the hinged_door 3d model from here :
   https://bitbucket.org/osrf/gazebo_models/downloads/

6) Copy the 3d folder file to
   /home/username/.gazebo

7) Go back to the project file, and go to its src folder and execute the 2 followings :
   chmod +x distance_listener.py
   chmod +x door_open_motion.py

8) Run gazebo with tiago simulation and the world using :
   roslaunch tiago_gazebo tiago_gazebo.launch public_sim:=true robot:=steel world:=office_with_door7

9) On new terminal Run the distance_listener.py by using the comamand :
   python distance_listener.py

10) On new terminal run  :
    python door_open_motion.py